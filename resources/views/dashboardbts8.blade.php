<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="row">
            <div class="card-body" style="height:210px; padding-top: 3px; ">
                <div class="chart" style="height:210px; background-color: #343a40;">
                    <canvas id="barChart8"></canvas>
                </div>
            </div>
        </div>
    </body>

    <script src="{{url('')}}/js/jquery.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'{{url("lackosong")}}',
            type:'get',
            success: function(result){
                console.log(result.hasil);

                var areaChartData = {
                    labels :result.regional,
                  datasets: [
                    {
                      label               : '2G',
                      fillColor           : '#fff',
                      strokeColor         : '#c2c7d0',
                      pointColor          : '#fff',
                      pointStrokeColor    : '#fff',
                      pointHighlightFill  : '#fff',
                      pointHighlightStroke: '#fff',
                      data                : result.hasil1
                    },
                    {
                      label               : '3G',
                      fillColor           : '#008080',
                      strokeColor         : '#c2c7d0',
                      pointColor          : '#008080',
                      pointStrokeColor    : '#008080',
                      pointHighlightFill  : '#008080',
                      pointHighlightStroke: '#008080',
                      data                : result.hasil2
                    },
                    {
                      label               : '4G',
                      fillColor           : '#808080',
                      strokeColor         : '#c2c7d0',
                      pointColor          : '#808080',
                      pointStrokeColor    : '#808080',
                      pointHighlightFill  : '#808080',
                      pointHighlightStroke: '#808080',
                      data                : result.hasil3
                    }
                  ]
                }
                //-------------
                //- BAR CHART -
                //-------------
                var barChartCanvas                   = $('#barChart8').get(0).getContext('2d')
                var barChart                         = new Chart(barChartCanvas)
                var barChartData                     = areaChartData
                var barChartOptions                  = {
                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                    scaleBeginAtZero        : true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines      : true,
                    //String - Colour of the grid lines
                    scaleGridLineColor      : '#0000',
                    scaleFontColor          :'#ffffff',
                    scaleLineColor          : '#ffffff',
                    //Number - Width of the grid lines
                    scaleGridLineWidth      : 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines  : true,
                    //Boolean - If there is a stroke on each bar
                    barShowStroke           : true,
                    //Number - Pixel width of the bar stroke
                    barStrokeWidth          : 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing         : 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing       : 1,
                    //String - A legend template
                    legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                    //Boolean - whether to make the chart responsive
                    responsive              : true,
                    maintainAspectRatio     : false
                }

                barChartOptions.datasetFill = false
                barChart.Bar(barChartData, barChartOptions)
            }
                    });
    });
    </script>
</html>
