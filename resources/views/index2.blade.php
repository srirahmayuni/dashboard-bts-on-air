<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('')}}/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.css">
  <link rel="stylesheet" href="{{url('')}}/plugins/bootstrap/css/bootstrap.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{url('')}}/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{url('')}}/plugins/morris/morris.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <!-- DataTables -->
  <link rel="stylesheet" href="{{url('')}}/plugins2/datatables/dataTables.bootstrap4.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('')}}/dist2/css/adminlte.min.css">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body class="hold-transition sidebar-mini">
  <div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
        </li>
        <li class="nav-item navbar-brand" style="color:white;">BTS ON AIR</li>
        <li class="nav-item">
          <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                  color: #000;
                  position: absolute;
                  font-size: 10px;
                  right: 6%;
                  height: auto;
                  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                  text-transform: uppercase;
                  font-family: Roboto;
                  padding: 1%;"><i class="fa fa-sign-out"></i> Log Out</a>
        </li>
      </ul>
    </nav>


  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{url('')}}/dist/img/tsel-white.png"
             style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
      </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item" >
            <a href="#" class="nav-link" style="background: #008080;">
              <i class="nav-icon fa fa-fire"></i>
                <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="http://10.54.36.49/dashboard-license" class="nav-link">
              <i class="nav-icon fa fa-fire"></i>
                <p>License</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="http://10.54.36.49/btsonair" class="nav-link">
              <i class="nav-icon fa fa-fire"></i>
                <p>BTS On Air</p>
            </a>
          </li>
          <li class="nav-item">
          <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
              <p>Nodin Integrasi</p>
          </a>
          </li>
          <li class="nav-item">
          <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link">
           <i class="nav-icon fa fa-fire"></i>
            <p>Nodin Rehoming</p>
          </a>
          </li>
          <li class="nav-item">
           <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
          <p>Nodin Dismantle</p>
          </a>
          </li>
          <li class="nav-item">
          <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link">
          <i class="nav-icon fa fa-fire"></i>
          <p>Nodin Relocation</p>
          </a>
          </li>
          <li class="nav-item">
            <a href="http://10.54.36.49/change-front-2/public/" class="nav-link">
              <i class="nav-icon fa fa-fire"></i>
            <p>Project Tracking</p>
          </a>
          </li>
          <li class="nav-item">
            <a href="http://10.54.36.49/tableList" class="nav-link">
              <i class="nav-icon fa fa-fire"></i>
                <p>Table List</p>
            </a>
          </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper" style="margin-top:55px;">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('index')}}">BTS On Air</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data Consistency</li>
      </ol>
  </nav>
    <section class="col-lg-12 col-lg-6">
      <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header" style="background-color: #008080;">
                <h3 style="color: white;" class="card-title">
                  <i class="fa fa-th mr-1"></i>
                NE ID Kosong
              </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="card-body" style="background-color: #343a40;">
               <center>
                  <label class="pull-center" style="color:white;">2G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;
                  <label class="pull-center" style="color:white;">3G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button>&nbsp;&nbsp;&nbsp;
                  <label class="pull-center" style="color:gray;">4G &nbsp;</label><button style="width: 12px; height: 12px; background-color: gray; border: 0px;"></button>
                </center>
                  @include('dashboardbts7')
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header" style="background-color: #008080;">
                <h3 style="color: white;" class="card-title">
                  <i class="fa fa-th mr-1"></i>
                LAC / TAC Kosong
              </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
              </div>
                <div class="card-body" style="background-color: #343a40;">
                   <center>
                    <label class="pull-center" style="color:white;">2G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;
                  <label class="pull-center" style="color:white;">3G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button>&nbsp;&nbsp;&nbsp;
                  <label class="pull-center" style="color:gray;">4G &nbsp;</label><button style="width: 12px; height: 12px; background-color: gray; border: 0px;"></button>
                  </center>
                        @include('dashboardbts8')
                </div>
              </div>
          </div>
      </div>
    </section>
    <section class="col-lg-12 col-lg-6">
      <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header" style="background-color: #008080;">
              <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>CI Kosong</h3>
              <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                      <i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove">
                      <i class="fa fa-times"></i>
                  </button>
              </div>
            </div>
            <div class="card-body" style="background-color: #343a40;">
              <center>
                    <label class="pull-center" style="color:white;">2G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;
                  <label class="pull-center" style="color:white;">3G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button>&nbsp;&nbsp;&nbsp;
                  <label class="pull-center" style="color:gray;">4G &nbsp;</label><button style="width: 12px; height: 12px; background-color: gray; border: 0px;"></button>
                  </center>
                    @include('dashboardbts9')
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header" style="background-color: #008080;">
              <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Enodeb ID 4G</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
              </div>
              <div class="card-body" style="background-color: #343a40;">
                      @include('dashboardbts10')
              </div>
            </div>
        </div>
      </div>
    </section>
  <section class="col-lg-12 col-lg-6">
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
         <div class="card-header" style="background-color: #008080;">
            <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Traffic LAC</h3>
             <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
        <div class="card-body" style="background-color: #343a40;">
        <center>
          <label class="pull-center" style="color:white;">2G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;
            <label class="pull-center" style="color:white;">3G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button>&nbsp;&nbsp;&nbsp;
            <label class="pull-center" style="color:gray;">4G &nbsp;</label><button style="width: 12px; height: 12px; background-color: gray; border: 0px;"></button>
          </center>
            @include('traffic_lac')
        </div>
          </div>
          </div>
          <div class="col-lg-6">
          <div class="card">
          <div class="card-header" style="background-color: #008080;">
            <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Traffic CI</h3>
            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
            </div>
          </div>
          <div class="card-body" style="background-color: #343a40;">
            <center>
              <label class="pull-center" style="color:white;">2G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;
              <label class="pull-center" style="color:white;">3G &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button>&nbsp;&nbsp;&nbsp;
              <label class="pull-center" style="color:gray;">4G &nbsp;</label><button style="width: 12px; height: 12px; background-color: gray; border: 0px;"></button>
            </center>
              @include('traffic_ci')
           </div>
           </div>
           </div>
   <section class="col-lg-12 col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-color: #008080;">
                                                    <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Data Salah</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    @include('dashboardbts11')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-color: #008080;">
                                                    <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Data Salah</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    @include('dashboardbts12')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
<footer class="main-footer">
  <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
</footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<script src="{{url('')}}/plugins2/jquery/jquery.min.js"></script>
<script src="{{url('')}}/plugins2/datatables/jquery.dataTables.js"></script>
<script src="{{url('')}}/plugins2/datatables/dataTables.bootstrap4.js"></script>
<script src="{{url('')}}/plugins2/slimScroll/jquery.slimscroll.min.js"></script>
<script src="{{url('')}}/plugins2/fastclick/fastclick.js"></script>
<script src="{{url('')}}/dist2/js/adminlte.min.js"></script>
<script src="{{url('')}}/dist2/js/demo.js"></script>

<script type="text/javascript">
  function logout(){
    sessionStorage.remove('token', '');
    window.location.href = "{{env('APP_URL')}}/landingPage/";
  }
</script>
<script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="{{url('')}}/js/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script>
$("#nav-open").click(function(){
    if (document.getElementById("wrapper").style.padding == "0px") {
        $("#wrapper").css('padding-left', '150px');
        $("#sidebar-wrapper").css('width', '150px');
    }else {
        document.getElementById("wrapper").style.padding = "0";
        document.getElementById("sidebar-wrapper").style.width = "0";
    }
});

$(document).ready(function(){
    $.ajax({
        url:"http://10.54.36.49/bts-rekon/api/get_summary_all",
        type:"get",
        success: function(result){
            var hasil = JSON.parse(result);
            $('#total_on_2g').html(hasil.total_on_2g);
            $('#new_2g').html(hasil.new_2g);
            $('#existing_2g').html(hasil.existing_2g);
            $('#dismantle_2g').html(hasil.dismantle_2g);
            $('#total_on_3g').html(hasil.total_on_3g);
            $('#new_3g').html(hasil.new_3g);
            $('#existing_3g').html(hasil.existing_3g);
            $('#dismantle_3g').html(hasil.dismantle_3g);
            $('#total_on_4g').html(hasil.total_on_4g);
            $('#new_4g').html(hasil.new_4g);
            $('#existing_4g').html(hasil.existing_4g);
            $('#dismantle_4g').html(hasil.dismantle_4g);
            $('#total_on_total').html(hasil.total_on_total);
            $('#new_total').html(hasil.new_total);
            $('#existing_total').html(hasil.existing_total);
            $('#dismantle_total').html(hasil.dismantle_total);
            console.log(hasil);
        }
    });
    $.ajax({
        url:"http://10.54.36.49/api-btsonair/public/api/bts_summary_2",
        type:"get",
        success: function(result){
            $('#resume_sysinfo').empty();
            for (var i = 0; i < result.length; i++) {
                console.log(result[i].regional);
                $('#resume_sysinfo').append('<tr><td>'+result[i].regional+'</td><td>'+result[i].data1+'</td><td>'+result[i].data2+'</td><td>'+result[i].data3+'</td><td>'+result[i].data4+'</td><td>'+result[i].data5+'</td></tr>');
            }
        }
    });
  //2g
    // $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/total_on_2g',
    //     type:'get',
    //     success: function(result){
    //         $('#total_on_2g').html(result);
    //
    //     }
    // });
    // $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/new_2g',
    //     type:'get',
    //     success: function(result){
    //         $('#new_2g').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    //     $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/existing_2g',
    //     type:'get',
    //     success: function(result){
    //       console.log(result);
    //         $('#existing_2g').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    // $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/dismantle_2g',
    //     type:'get',
    //     success: function(result){
    //         $('#dismantle_2g').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    //
    // //3g
    //  $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/total_on_3g',
    //     type:'get',
    //     success: function(result){
    //         $('#total_on_3g').html(result);
    //
    //     }
    // });
    // $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/new_3g',
    //     type:'get',
    //     success: function(result){
    //         $('#new_3g').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    //     $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/existing_3g',
    //     type:'get',
    //     success: function(result){
    //       console.log(result);
    //         $('#existing_3g').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    // $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/dismantle_3g',
    //     type:'get',
    //     success: function(result){
    //         $('#dismantle_3g').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    //
    // //4g
    //  $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/total_on_4g',
    //     type:'get',
    //     success: function(result){
    //         $('#total_on_4g').html(result);
    //
    //     }
    // });
    // $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/new_4g',
    //     type:'get',
    //     success: function(result){
    //         $('#new_4g').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    //     $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/existing_4g',
    //     type:'get',
    //     success: function(result){
    //       console.log(result);
    //         $('#existing_4g').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    // $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/dismantle_4g',
    //     type:'get',
    //     success: function(result){
    //         $('#dismantle_4g').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    //
    // //total
    //  $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/total_on_total',
    //     type:'get',
    //     success: function(result){
    //         $('#total_on_total').html(result);
    //
    //     }
    // });
    // $.ajax({
    //     url:'http://localhost/apis-bts-rekon/api/new_total',
    //     type:'get',
    //     success: function(result){
    //         $('#new_total').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    //     $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/existing_total',
    //     type:'get',
    //     success: function(result){
    //       console.log(result);
    //         $('#existing_total').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
    // $.ajax({
    //      url:'http://localhost/apis-bts-rekon/api/dismantle_total',
    //     type:'get',
    //     success: function(result){
    //         $('#dismantle_total').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
    //     }
    // });
});
</script>
<script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{url('')}}/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{url('')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('')}}/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{url('')}}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="{{url('')}}/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{url('')}}/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{url('')}}/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('')}}/dist/js/demo.js"></script>
<script src="{{url('')}}/plugins/chartjs-old/Chart.min.js"></script>
</body>
</html>
